#!/usr/bin/php
<?php
require_once('simple_html_dom.php');
mb_internal_encoding("UTF-8");
mb_regex_encoding("UTF-8");
function htrim($str) {
        $str = trim($str);
        $str = str_replace("&nbsp;", "", $str);
        $str = str_replace("<", "＜", $str);
        $str = str_replace(">", "＞", $str);
        return $str;
}

$DVDNO = 1;
$tree = array();

start:

if (!file_exists("DVD{$DVDNO}/mayaleft.html")) {

	//die(var_dump($tree));
	
	$count = 0;
	$SCRIPT = "#!/bin/bash\nset -e\n\n";
	foreach ($tree as $k1 => $t1) {
		if (!isset($t1['sub'])) {
			$t1['sub'] = array(
				'X' => array(
					'name' => $t1['name'],
					'link' => $t1['link']
				)
			);
		}
		foreach ($t1['sub'] as $k2 => $t2) {
			if ($k2=='X') {
				$title = htrim('第'.intval(trim($k1,'_')).'章 '.$t1['name']);
				$title2 = htrim('第'.intval(trim($k1,'_')).'章 '.$t1['name']);
			} else {
				$title = htrim('第'.intval(trim($k1,'_')).'章第'.intval(trim($k2,'_')).'节 '.$t2['name']);
				$title2 = htrim('第'.intval(trim($k1,'_')).'章 '.$t1['name'].' - 第'.intval(trim($k2,'_')).'节 '.$t2['name']);
			}
			$t2html = file_get_html($t2['link']);
			$tdstr = null;
			foreach ($t2html->find('td') as $td) {
				if ($tdstr !==null) {
					$tdstr = trim($td->plaintext);
					$tdstr = mb_ereg_replace('[\s　]+', '', $tdstr);
					break;
				}
				if (trim($td->plaintext)=='内容简介') {
					$tdstr = '';
				}
			}
			if (strlen($tdstr)<20) die("desc is too short.\n");
			$description = htrim($tdstr);
			$basename = '';
			foreach ($t2html->find('a') as $anchor) {
				if (preg_match('/^video/i', trim($anchor->href))) {
					$basename = trim($anchor->href);
					break;
				}
			}
			if ($basename!='') $basename = dirname($t2['link']).'/'.$basename;
			if (strlen($basename)<20) die("basename is too short.\n");
			$t3 = $t2['link'];

			$SCRIPT .= "(printf \"《火星人——Maya火星课堂》\\n{$title2}\\n{$description}\\n\\n".
			"http://got.notsh.it/maya-classroom\" | ".
			"youtube-upload --email=guesswhatihate@gmail.com ".
			"--password= --category=Education --keywords=maya ".
			"--title=\"{$title} [火星人——Maya火星课堂]\" ".
			"--description=\"$(< /dev/stdin)\" \"{$basename}\" | ".
			"sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\\1/' - | tr -d \"\\n\" && ".
			"printf \" => {$t3}\\n\") >> ./log && tail -n 1 ./log;\n\nsleep 5;\n\n".
			"(printf \"http://www.youtube.com/watch?v=\" && tail -n 1 ./log | cut -c 1-11) | ".
			"youtube-upload --email=guesswhatihate@gmail.com --password= ".
			"--add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBFkkjTYNB9y0uLycX7Hnbs $(< /dev/stdin)\n\nsleep 5;\n\n";
			
			$count+=1;
		}
	}

	file_put_contents('bash', $SCRIPT);
	echo "Total {$count} items.\n";
	die("END.\n");
}

$html = file_get_contents("DVD{$DVDNO}/mayaleft.html");

$html = str_get_html($html);

/*
// if you are starting from DVD 4, make sure to include info of last chapter.
if ($DVDNO==4) {
	$tree['09_'] = array(
		'name' => 'Maya人体建模全案',
		'link' => null
	);
	
}
*/

foreach ($html->find('td') as $td) {
	$tdc = trim($td->plaintext);
	$tda = $td->find('a', 0);
	if ($tda!=null) $tda = $tda->href;
	if (preg_match('/^(\d{2}_)(\d{2}_)(.*)$/', $tdc, $mat)) {
		if (!isset($tree[$mat[1]]['sub']))
			$tree[$mat[1]]['sub'] = array();
		if (isset($tree[$mat[1]])) {
			$tree[$mat[1]]['sub'][$mat[2]] = array(
				'name' => $mat[3],
				'link' => "DVD{$DVDNO}/{$tda}"
			);
		} else {
			die('some error');
		}
	} else if (preg_match('/^(\d{2}_)(.*)$/', $tdc, $mat)) {
		$tree[$mat[1]] = array(
			'name' => $mat[2],
			'link' => "DVD{$DVDNO}/{$tda}"
		);
	}
}

$DVDNO++;

goto start;
