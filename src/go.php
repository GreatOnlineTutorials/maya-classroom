#!/usr/bin/php
<?php

$logs = file_get_contents("log");

$count = 0;

foreach (explode("\n", $logs) as $l) {
        $s = explode(" => ", $l);
        if (isset($s[1]) && preg_match("/^DVD/", $s[1])) {
                $c = file_get_contents($s[1]);
                $c = preg_replace('/href="video(.+?)"/i', 'href="http://www.youtube.com/embed/'.$s[0].'"', $c);
                file_put_contents($s[1], $c);
                $count++;
        }
}

echo "{$count} HTML files changed.\n";
